const { response, request } = require("express");
const express = require("express");

const db = require("../db");
const utils = require("../utils");

const router = express.Router();

// POST --> ADD Book data

router.post("/", (request, response) => {
  const { book_title, publisher_name, author_name } = request.body;

  const query = `
    INSERT INTO BOOK
    (book_title, publisher_name, author_name)
    VALUES
    ('${book_title}','${publisher_name}','${author_name}')
  `;

  db.execute(query, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// GET  --> Display Book using the name
//  finding book by its name

router.get("/:name", (request, response) => {
  const { name } = request.params;

  const query = `
    SELECT 
    book_id, book_title, publisher_name, author_name
    FROM BOOK 
    WHERE book_title = '${name}'
    
    `;
  db.execute(query, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// UPDATE --> Update publisher_name and Author_name
// update details by book id

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { publisher_name, author_name } = request.body;

  const query = `
    UPDATE BOOK 
    SET
    publisher_name='${publisher_name}',
    author_name = '${author_name}'
    WHERE book_id= ${id}
  `;

  db.execute(query, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

//  DELETE --> Delete Book
//  DELETE book by book id
router.delete("/:id", (request, response) => {
  const { id } = request.params;

  const query = `
    DELETE FROM BOOK
    WHERE book_id= ${id}
  `;

  db.execute(query, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

module.exports = router;

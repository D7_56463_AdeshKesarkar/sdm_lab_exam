const { response } = require("express");
const express = require("express");
const routerBook = require("./routes/book");
const app = express();

app.use(express.json());

app.use("/book", routerBook);

response.send("Welcome to the lab exam")

app.listen(4000, "0.0.0.0", () => {
  console.log("server started at port 4000");
});
